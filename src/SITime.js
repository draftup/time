// @flow

import SIPrefix from "./SIPrefix";
import { SIDeca } from "./SIPrefixes";
import SIQuantity from "./SIQuantity";
import { SIDay, SIHour, SIMinute, SISecond } from "./SITimeUnits";
import SIUnit from "./SIUnit";

export default class SITime extends SIQuantity {
  getSeconds(): number {
    return this.baseUnit.getValue();
  }

  getMintues(): number {
    return this.baseUnit.getValue() / SITime.BASE_UNITS_IN_MINUTE;
  }

  getHours(): number {
    return this.baseUnit.getValue() / SITime.BASE_UNITS_IN_HOUR;
  }

  getDays(): number {
    return this.baseUnit.getValue() / SITime.BASE_UNITS_IN_DAY;
  }

  setSeconds(seconds: number): SITime {
    this.baseUnit.setValue(seconds);

    return this;
  }

  setMintues(minutes: number): SITime {
    this.baseUnit.setValue(minutes * SITime.BASE_UNITS_IN_MINUTE);

    return this;
  }

  setHours(hours: number): SITime {
    this.baseUnit.setValue(hours * SITime.BASE_UNITS_IN_HOUR);

    return this;
  }

  setDays(days: number): SITime {
    this.baseUnit.setValue(days * SITime.BASE_UNITS_IN_DAY);

    return this;
  }

  static BASE_UNIT_CONSTRUCTOR: Class<SIUnit> = SISecond;

  static BASE_UNITS_IN_SECOND: number = 1;

  static BASE_UNITS_IN_MINUTE: number = SITime.BASE_UNITS_IN_SECOND * 60;

  static BASE_UNITS_IN_HOUR: number = SITime.BASE_UNITS_IN_MINUTE * 60;

  static BASE_UNITS_IN_DAY: number = SITime.BASE_UNITS_IN_HOUR * 24;

  static parseUnit(unit: SIUnit): SITime {
    const getBaseValue = () => {
      switch (unit.constructor.SYMBOL) {
        case SIMinute.SYMBOL:
          return unit.getValue() * SITime.BASE_UNITS_IN_MINUTE;
        case SIHour.SYMBOL:
          return unit.getValue() * SITime.BASE_UNITS_IN_HOUR;
        case SIDay.SYMBOL:
          return unit.getValue() * SITime.BASE_UNITS_IN_DAY;
        default:
          throw new TypeError(`Failed to parse time unit ${unit.toString()}`);
      }
    };

    const baseValue = getBaseValue();

    return new SITime(baseValue);
  }
}

const decaDays =
  new SITime().setSeconds(10).setMintues(20).baseUnit.getValue() /
  SIDeca.DECIMAL;
