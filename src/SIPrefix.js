// @flow

export default class SIPrefix {
  static SYMBOL: string;

  static DECIMAL: number;
}
