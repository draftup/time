// @flow

import SIPrefix from "./SIPrefix";

export default class SIUnit {
  #value: number = 0;

  constructor(initialValue?: number) {
    if (initialValue !== undefined) {
      this.#value = initialValue;
    }
  }

  getValue(): number {
    return this.#value;
  }

  setValue(value: number): SIUnit {
    this.#value = value;

    return this;
  }

  toString(): string {
    return `${this.getValue()} ${this.constructor.SYMBOL}`;
  }

  static SYMBOL: string;
}
