// @flow

import SIPrefix from "./SIPrefix";
import SIUnit from "./SIUnit";

export class SISecond extends SIUnit {
  static SYMBOL: string = "s";
}

export class SIMinute extends SIUnit {
  static SYMBOL: string = "min";
}

export class SIHour extends SIUnit {
  static SYMBOL: string = "h";
}

export class SIDay extends SIUnit {
  static SYMBOL: string = "d";
}
