// @flow

import SIUnit from "./SIUnit";

export default class SIQuantity {
  baseUnit: SIUnit;

  constructor(initialValue?: number) {
    const { BASE_UNIT_CONSTRUCTOR } = this.constructor;

    this.baseUnit = new BASE_UNIT_CONSTRUCTOR(initialValue);
  }

  static BASE_UNIT_CONSTRUCTOR: Class<SIUnit>;

  static parseUnit(unit: SIUnit): SIQuantity {
    return new SIQuantity(unit.getValue());
  }
}
