// @flow

import SIPrefix from "./SIPrefix";

export class SINano extends SIPrefix {
  static SYMBOL: string = "n";

  static DECIMAL: number = 0.000000001;
}

export class SIMicro extends SIPrefix {
  static SYMBOL: string = "μ";

  static DECIMAL: number = 0.000001;
}

export class SIMilli extends SIPrefix {
  static SYMBOL: string = "m";

  static DECIMAL: number = 0.001;
}

export class SICenti extends SIPrefix {
  static SYMBOL: string = "c";

  static DECIMAL: number = 0.01;
}

export class SIDeci extends SIPrefix {
  static SYMBOL: string = "d";

  static DECIMAL: number = 0.1;
}

export class SIDeca extends SIPrefix {
  static SYMBOL: string = "da";

  static DECIMAL: number = 10;
}

export class SIHecto extends SIPrefix {
  static SYMBOL: string = "h";

  static DECIMAL: number = 100;
}

export class SIKilo extends SIPrefix {
  static SYMBOL: string = "k";

  static DECIMAL: number = 1000;
}

export class SIMega extends SIPrefix {
  static SYMBOL: string = "M";

  static DECIMAL: number = 1000000;
}

export class SGiga extends SIPrefix {
  static SYMBOL: string = "G";

  static DECIMAL: number = 1000000000;
}
